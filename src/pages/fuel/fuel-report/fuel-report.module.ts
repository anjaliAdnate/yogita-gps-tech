import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelReportPage } from './fuel-report';

@NgModule({
  declarations: [
    FuelReportPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelReportPage),
  ],
})
export class FuelReportPageModule {}
