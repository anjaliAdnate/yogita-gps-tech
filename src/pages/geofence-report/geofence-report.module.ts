import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeofenceReportPage } from './geofence-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    GeofenceReportPage,
  ],
  imports: [
    IonicPageModule.forChild(GeofenceReportPage),
    SelectSearchableModule
  ],
})
export class GeofenceReportPageModule {}
