import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-fuel-consumption-report',
  templateUrl: 'fuel-consumption-report.html',
})
export class FuelConsumptionReportPage {
  islogin: any;
  portstemp: any[] = [];
  datetimeStart: any;
  datetimeEnd: any;
  vehId: any;
  _listData: any[] = [];
  fuelDataArr: any[] = [];
  eventT: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuelConsumptionReportPage');
  }
  ngOnInit() {
    this.getdevices();
  }

  getId(veh) {
    debugger
    this.vehId = veh.Device_ID;
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getReport() {
    if (this.vehId == undefined) {
      let toast = this.toastCtrl.create({
        message: "Please Select Device ... ",
        duration: 1500,
        position: "bottom"
      })
      toast.present();
    } else {
      var _baseURL = "https://www.oneqlik.in/notifs/detFLRep?t=" + new Date(this.datetimeEnd).toISOString() + "&f=" + new Date(this.datetimeStart).toISOString() + "&i=" + this.vehId;
      this.apiCall.startLoading().present();
      this.apiCall.getSOSReportAPI(_baseURL)
        .subscribe(res => {
          this.apiCall.stopLoading();
          // console.log(data)
          this.fuelDataArr = [];
          if (res.result.length > 2) {
            var add1 = res.result[1].address ? res.result[1].address.split(",") : "";
            var addr1 = "";
            for (var j = 2; j < (add1.length - 1); j++) {
              add1[j] = add1[j];
              addr1 = (addr1 ? addr1 : "") + (add1[j] ? add1[j] : "");
            }
            var obj = {
              "date_time": "At the start of the  period",
              "tank_surplus": res.result[0].currentFuel ? (res.result[0].currentFuel).toFixed(3) : "",
              "location": addr1 ? addr1 : ""
            }
            this.fuelDataArr.push(obj);
            var totalFuelIn = 0.0;
            var totalDuelOut = 0.0;
            var totalFuelUsed = 0.0;
            var totalDistance = 0.0;
            for (var i = 1; i < (res.result.length - 1); i++) {
              // console.log("in fu");
              var tank_surplus_bef: any;
              var fuel: any
              if (res.result[i].pour == 'OUT') {
                tank_surplus_bef = (res.result[i].litres + res.result[i].currentFuel).toFixed(3);
                fuel = "-" + res.result[i].litres
              }
              else if (res.result[i].pour == 'IN') {
                tank_surplus_bef = (res.result[i].currentFuel - res.result[i].litres).toFixed(3);
                fuel = res.result[i].litres
              }

              var t1 = moment(res.result[i].timestamp).format('DD/MM/YYYY, h:mm:ss a')
              var arr = t1.split(",");
              var add = res.result[i].address ? res.result[i].address.split(",") : "";
              var addr = "";
              for (var j = 2; j < (add.length - 1); j++) {
                add[j] = add[j];
                addr = addr ? addr : "" + add[j] ? add[j] : "";
              }
              this.eventT = res.result[i].pour;
              var tanksurplus = 0;
              if (res.result[i].currentFuel != 0) {
                tanksurplus = res.result[i].currentFuel;
              }

              var p_obj = {
                "date_time": arr[0] + "," + arr[1],
                "drive_time": this.secondsToHms((new Date(res.result[i].timestamp).getTime() - new Date(res.result[i - 1].timestamp).getTime()) / 1000),
                "tank_surplus_before": tank_surplus_bef,
                "fuel_used": (res.result[i - 1].currentFuel - tank_surplus_bef).toFixed(3),
                "fuel_change": fuel,
                "tank_surplus": tanksurplus.toFixed(3),
                "distance": ((res.result[i].odo - res.result[i - 1].odo) != NaN) ? (res.result[i].odo - res.result[i - 1].odo).toFixed(3) : "N/A",
                "event_type": res.result[i].pour,
                "location": addr ? addr : "N/A",
              }

              // console.log("inside value of distance", p_obj.distance);
              totalFuelUsed = totalFuelUsed + Number((res.result[i - 1].currentFuel - tank_surplus_bef).toFixed(3));
              totalDistance = totalDistance + Number(p_obj.distance);
              if (res.result[i].pour == 'OUT') {
                totalFuelIn = totalFuelIn + res.result[i].litres;

              }
              if (res.result[i].pour == 'IN') {
                totalDuelOut = totalDuelOut + res.result[i].litres;

              }
              this.fuelDataArr.push(p_obj);
              // console.log(p_obj);
            }
            var len = res.result.length
            var add2 = res.result[len - 2].address ? res.result[len - 2].address.split(",") : "";
            var addr2 = "";
            for (var j = 2; j < (add2.length - 1); j++) {
              add2[j] = add2[j];
              addr2 = (addr2 ? addr2 : "") + (add2[j] ? add2[j] : "");
            }
            var obj1 = {
              "date_time": "At the end  of the period",
              "drive_time": this.secondsToHms((new Date(res.result[len - 1].timestamp).getTime() - new Date(res.result[len - 2].timestamp).getTime()) / 1000),
              "tank_surplus": (res.result[len - 1].currentFuel) ? (res.result[len - 1].currentFuel).toFixed(3) : '',
              "distance": ((res.result[len - 1].odo - res.result[len - 2].odo) != NaN) ? (res.result[len - 1].odo - res.result[len - 2].odo).toFixed(3) : "NA",
              "location": addr2 ? addr2 : addr2
            }
            this.fuelDataArr.push(obj1);

            var obj2 = {
              "date_time": "Total",
              "fuel_used": totalFuelUsed.toFixed(3),
              "fuel_change": '-' + totalFuelIn.toFixed(3) + '  ,' + totalDuelOut.toFixed(3),
              "distance": (totalDistance != NaN) ? totalDistance.toFixed(3) : "NA"

            }
            this.fuelDataArr.push(obj2);
            console.log(this.fuelDataArr)

          }
          // if (data.message == "Success") {
          //   this._listData = data.result;
          // }
        },
          err => {
            console.log(err)
            this.apiCall.stopLoading();
          })
    }

  }
  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
    return hDisplay + mDisplay + sDisplay;
  }
}
